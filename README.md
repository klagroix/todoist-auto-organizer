# todoist-auto-organizer

This will watch a todoist project and learn which sections items are placed in. When the same item is placed in the future, the item will automatically be moved to the appropriate section. This is especially useful for a grocery list.

## Usage

```
docker run -d \
 --name=todoist-auto-organizer \
 -e API_TOKEN=<APITOKEN> \
 -e PROJECT_NAME=<PROJECT> \
 -v todoist_org_data:/data
 jovocop/todoist-auto-organizer
 ```

### Environment variables

* `API_TOKEN` - Your todoist API token. To retrieve this, open Todoist Settings -> Integrations -> API token
* `PROJECT_NAME` - The name of the project in todoist to organize
* (optional) `IGNORE_CHECKED` - Default: True. Set to False if you want to organize already checked items as well
* (optional) `RUN_FREQUENCY_MIN` - Default: 10. Change this to the desired check interval (in minutes)

##  Build

GitLab CI automatically deploys new containers to https://hub.docker.com/repository/docker/jovocop/todoist-auto-organizer

To build this manually, run `docker build -t todoist-auto-organizer .`


## Libraries
Third Party libraries required are documented in `requirements.txt`

NOTE: This uses a forked library here (https://github.com/klagroix/todoist-python) which allows for moving items to different sections

## References

The following guides were used:
* https://developer.todoist.com/sync/v8/?python#getting-started
