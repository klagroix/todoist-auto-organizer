#!/usr/bin/env python

# Guides

"""This will watch a todoist project and learn which sections items are placed in. When the same item is placed in the future, the item will automatically be moved to the appropriate section. This is especially useful for a grocery list.
"""

from todoist.api import TodoistAPI
import sqlite3
import schedule
import os
import sys
import datetime
import time

TOOL_NAME = "todoist-auto-organizer"
DATABASE_PATH = "/data/todoist-auto-organizer.db"

API_TOKEN = os.environ.get('API_TOKEN') # Todoist Settings -> Integrations -> API token
PROJECT_NAME = os.environ.get('PROJECT_NAME')
IGNORE_CHECKED = os.environ.get('IGNORE_CHECKED', True) # If True, we'll only process items that are not checked (i.e. still active in the list)
RUN_FREQUENCY_MIN = os.environ.get('RUN_FREQUENCY_MIN', 10) # How often we should run the check

# Source: https://stackoverflow.com/questions/3300464/how-can-i-get-dict-from-sqlite-query
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def connect_db():
    # Connect to local db
    db_conn = sqlite3.connect(DATABASE_PATH)
    db_conn.row_factory = dict_factory
    return db_conn


def create_tables(cursor):
    """
    CREATES TABLES if they don't exiwst
    """
    cursor.execute("""CREATE TABLE IF NOT EXISTS items (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        section_name TEXT NOT NULL,
        section_id INTEGER NOT NULL
    )
    """)

def db_get_all_items(cursor):
    cursor.execute("SELECT * from items")
    return cursor.fetchall()

def record_item_sections(section_id, section_name, items):
    db_conn = connect_db()
    cursor = db_conn.cursor()
    known_items = db_get_all_items(cursor)

    for item in items:
        matches = list(filter(lambda known_item: known_item['name'].lower() == item["content"].lower(), known_items))  # See if there's a known item in the database already
        if len(matches) > 0:
            # We have a match in then DB. let's compare section to confirm it matches
            match = matches[0]  # We expect to only find one match
            if item["section_id"] == match["section_id"]:
                print("Not modifying {0} as we already know it's in section id {1} ({2})".format(item["content"], match["section_id"], match["section_name"]))
            else:
                print("Updating section of {0} to now be {1}/{2} (was: {3}/{4})".format(item["content"], section_id, section_name, match["section_id"], match["section_name"]))
                cursor.execute("UPDATE items set section_id=?, section_name=? where name=?", (section_id, section_name, match["name"]))
        else:
            # We don't know about this item yet. Lets save a new row
            print("Adding a new row for item: {0}".format(item["content"]))
            cursor.execute("INSERT INTO items (name, section_name, section_id) values (?, ?, ?)", (item["content"], section_name, section_id))
    db_conn.commit()
    db_conn.close()

def categorize_items(items):
    """Takes a list of uncategorized items from the API and tries to sort them"""
    db_conn = connect_db()
    cursor = db_conn.cursor()
    known_items = db_get_all_items(cursor)
    
    changes = 0
    for item in items:
        matches = list(filter(lambda known_item: known_item['name'].lower() == item["content"].lower(), known_items))  # See if there's a known item in the database already
        if len(matches) > 0:
            # We have a match in then DB. let's update the item to be in the proper section
            section_id = matches[0]["section_id"]
            section_name = matches[0]["section_name"]
            print("Updating item {0} to have a section of {1}/{2}".format(item["content"], section_id, section_name))
            item.move(section_id=section_id)
            changes += 1

    print("Categorized {0} items".format(changes))
    if changes > 0:
        api.commit()
    db_conn.close()

def get_project(name, case_sensitive=False):
    """Returns a project dictionary (from the API) which matches the provided name"""
    projects = api.state['projects']

    for c_project in projects:
        # print("PROJECT: {0}".format(c_project))
        # print("Name: {0}".format(c_project["name"]))

        proj_name = c_project["name"]

        if case_sensitive and proj_name == name:
            # print("Found match")
            return c_project
        elif not case_sensitive and proj_name.lower() == name.lower():
            # print("Found match")
            return c_project

    return None

def get_items_for_project(project_id):
    """Gets all items for a given project id"""
    proj_items = []

    for item in api.items.all():
        if item["project_id"] == project_id:
            if IGNORE_CHECKED and item["checked"]:
                continue
            proj_items.append(item)

    return proj_items

def get_sections_for_project(project_id):
    """Gets all sections for a given project id"""
    sections = []

    for section in api.sections.all():
        if section["project_id"] == project_id:
            if not section["is_deleted"] and not section["is_archived"]:
                sections.append(section)

    return sections

def get_items_in_section(section_id, item_list):
    """Returns all the items in a given section"""
    sec_items = []
    
    for c_item in item_list:
        if c_item["section_id"] == section_id:
            sec_items.append(c_item)
    return sec_items

def finish_pass():
    print("Finished {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    #sys.exit(0)
    return 0

def finish_fail():
    print("Finished {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    #sys.exit(1)
    return 1


def run():
    print("Entering {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    
    # Init the API
    print("Initializing the API...", end='')
    api.sync()
    print("Done")
    
    # Get the project we care about
    print("Getting project {0}...".format(PROJECT_NAME), end='')
    project = get_project(PROJECT_NAME)
    if project is None:
        print("ERROR: Couldn't find project")
        finish_fail()
    print("Done")
    
    project_id = project["id"]
    
    # Iterate through items in the project. Store a list of duplicates
    print("Getting all items for the project...", end='')
    items = get_items_for_project(project_id)
    print("Done")

    print("Getting all sections for the project...", end='')
    sections = get_sections_for_project(project_id)
    print("Done")
    
    # Loop through every item in each section and add it to the db if we don't know it's already there
    for section in sections:
        print("Processing section: {0}/{1}".format(section["id"], section["name"]))
        sec_items = get_items_in_section(section["id"], items)
        print("Found {0} items in the section".format(len(sec_items)))
        # Save to database
        record_item_sections(section["id"], section["name"], sec_items)

    # Organize uncategorized items
    print("Trying to sort uncategorized items")
    uncategorized_items = []
    for item in items:
        if item["section_id"] is None:
            # Uncategorized item. Lets see if we can categorize it
            uncategorized_items.append(item)
    categorize_items(uncategorized_items)

    finish_pass()

if API_TOKEN is None:
    print("ERROR: No API_TOKEN provided")
    sys.exit(1)

if PROJECT_NAME is None:
    print("ERROR: No PROJECT_NAME provided")
    sys.exit(1)


print("Starting container ({0}). Setting job to run every {1} minutes".format(datetime.datetime.now(), RUN_FREQUENCY_MIN))

db_conn = connect_db()
cursor = db_conn.cursor()
create_tables(cursor)
db_conn.commit()
db_conn.close()

# Set the API
api = TodoistAPI(API_TOKEN)

# Set the schedule
schedule.every(RUN_FREQUENCY_MIN).minutes.do(run)

# run schedules
while True:
    schedule.run_pending()
    time.sleep(1)